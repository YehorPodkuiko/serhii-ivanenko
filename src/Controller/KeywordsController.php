<?php
/**
 * Created by PhpStorm.
 * User: vantino
 * Date: 3/26/19
 * Time: 5:54 PM
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class KeywordsController extends AbstractController
{
    /**
     * @Route("/", name="keywords_index")
     */
    public function index()
    {
        $url = 'https://symfony.com/doc/3.4/doctrine.html';

        if (!$this->urlExists($url)) {
            return $this->render('keywords/index.html.twig', [
                'message' => 'URL is not exists',
            ]);
        }

        $title = $this->getPageTitle($url);
        $keywords = $this->titleToKeywords($title);

        return $this->render('keywords/index.html.twig', [
            'title' => $title,
            'keywords' => $keywords
        ]);
    }

    protected function urlExists($url)
    {
        $headers = @get_headers($url);
        if (strpos($headers[0], '404') === false) {
            return true;
        }
        return false;
    }

    protected function getPageTitle($url)
    {
        $str = file_get_contents($url);

        if (strlen($str) > 0) {
            $str = trim(preg_replace('/\s+/', ' ', $str));
            preg_match("/\<title\>(.*)\<\/title\>/i", $str, $title);
            return $title[1];
        }

        return null;
    }

    protected function titleToKeywords($title)
    {
        $title = preg_replace("/[^A-Za-z0-9 ]/", '', $title);

        $temp = explode(' ', $title);
        $keywords = [];

        $total = count($temp);

//        for ($i = 0, $total; $i < $total; $i++) {
//            for ($j = $total; $j > $i; $j--) {
//                $keywords[] = implode(' ', array_slice($temp, $i, $j));
//            }
//        }

        for ($i = 0; $i < $total; $i++) {
            $str = $temp[$i];
            $keywords[] = $str;

            for ($j = $i + 1; $j < $total; $j++) {
                $str = $str . ' ' . $temp[$j];
                $keywords[] = $str;
            }

        }

//        return array_unique(array_merge($keywords, $temp));
        return $keywords;
    }
}